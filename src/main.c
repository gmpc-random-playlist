/* gmpc-random-playlist (GMPC plugin)
 * Copyright (C) 2006-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <string.h>



#include <glade/glade.h>
#include <gmpc/plugin.h>
#include <libmpd/debug_printf.h>
#include <config.h>

/* External pointer + function, there internal from gmpc */
extern GladeXML *pl3_xml;
/** in gmpc */
void pl3_option_menu_activate(); 

static int get_enabled() {
	return cfg_get_single_value_as_int_with_default(config,"random-playlist", "enabled",TRUE);
}
static void set_enabled(int enabled) {
	cfg_set_single_value_as_int(config,"random-playlist", "enabled", enabled);
	pl3_option_menu_activate(); 
}
static void rp_start()
{
	MpdData *data = mpd_database_get_complete(connection);
	GRand *rand = g_rand_new();
	int value, pos = cfg_get_single_value_as_int_with_default(config, "random-playlist", "addfact",20)*100;
	mpd_playlist_clear(connection);
	for(;data;data = mpd_data_get_next(data))
	{
		value = g_rand_int_range(rand, 0,10000);
		/** add 20% */
		if(value < pos)
		{
			mpd_playlist_queue_add(connection, data->song->file);
		}
	}
	mpd_playlist_queue_commit(connection);
	if(cfg_get_single_value_as_int_with_default(config, "random-playlist", "shuffle",TRUE))
	{
		mpd_playlist_shuffle(connection);
	}
	g_rand_free(rand);
}


static int rp_right_mouse_menu(GtkWidget *menu, int type, GtkWidget *tree, GdkEventButton *event)
{
	gmpcPlugin *plug = plugin_get_from_id(type);
	if(!cfg_get_single_value_as_int_with_default(config, "random-playlist", "enabled", TRUE)) {
		return 0;
	}
	debug_printf(DEBUG_INFO,"Random playlist right mouse clicked");	
	if(!strcmp(plug->name, "Current Playlist Browser"))
	{
		GtkWidget *item;
		item = gtk_image_menu_item_new_with_label("Generate Random Playlist");
		gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item),
				gtk_image_new_from_stock(GTK_STOCK_ADD, GTK_ICON_SIZE_MENU));
		gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
		g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(rp_start), NULL);
		return 1;
	}
	return 0;
}



gmpcPlBrowserPlugin rp_gpb  = {
    .cat_right_mouse_menu = rp_right_mouse_menu
};


gmpcPlugin plugin = {
	.name           = "Random Playlist",
	.version        = {PLUGIN_MAJOR_VERSION,PLUGIN_MINOR_VERSION,PLUGIN_MICRO_VERSION},
	.plugin_type    = GMPC_PLUGIN_PL_BROWSER,
	.browser        = &rp_gpb, /* browser intergration */
	.get_enabled    = get_enabled,
	.set_enabled    = set_enabled
};
int plugin_api_version = PLUGIN_API_VERSION;
